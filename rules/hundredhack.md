# The Hundred Hack

Inspired by David Black's incredible The Black Hack and Newt Newport's unbelievable Open Quest.

----

## What’s This?

The Hundred Hack (100H) is a traditional tabletop roleplaying game, played with paper, pencils and dice - it uses OpenQuest as a base.
But it adds and takes away elements to make it a distinct streamlined flavor of the original roleplaying game.

## The Core Mechanic

Everything a character might possibly attempt that could result in failure is resolved by making tests, in order to successfully pass a test a player must roll **below** their characteristic score on a D100.
Some tests are easy, doubling your chance of success.
Other tests are hard, halving your chance of success.

If the result of the test is less than 1/10 the test's goal, it is a critical success - the _best possible_ result based on the character's intentions.

If the result of the test is 100, it is a fumble - the _worst possible_ result based on the character's intentions.

Monsters don’t make tests - a character must avoid their attacks by making a test, the only time a monster would roll is for damage.

### Opposed Tests
Opposed tests are made by both sides who are in direct competition with each other.
Both characters make the tests as normal and the results depend on how well both sides do:

+ **One Character Succeeds:**
  If one character succeeds their test and the other fails then the successful character has won the opposed test.
+ **Both Characters Succeed:**
  If both characters succeed then whoever rolled the highest in their test wins the opposed test.
  However, if one character rolls a critical while the other rolls an ordinary success then the character that rolled the critical wins.
+ **Both Characters Fail:**
  Whoever rolled the lowest in their test wins the opposed test.
  In the case of ties for both the Player wins.
  If the action is player-against-player, the defender wins.

## Rolling Characteristics

**Characteristics** are generated with 3d6 in the following order **Body (BOD), Dexterity (DEX), Instinct (INS), Reason (REA), Empathy (EMP), and Power (POW)**.
Once all characteristics are generated 2 may be swapped around.

The base score for tests is 3x the characteristic.
Declare _what_ you're trying to accomplish **and** _how_.
The GM will determine if your action requires a test and which characteristic is most appropriate.

Then you'll try to roll under the appropriate score.

## Determining Attributes

+ **Damage Bonus (DB):** When attacking, characters add +1 damage for every four points of their relative characteristic.
  For example, if attacking with an axe and a BOD of 15, the character would have a +3 damage bonus; if shooting a bow with a DEX of 12, a +3 damage bonus.
+ **Hit Points (HP):** Add your characters BOD and POW, divide by two (round up) to determine HP.
  For example, with a BOD of 12 and a POW of 9, the character would have 11HP; with a BOD of 10 and a POW of 6, 8HP.
+ **Power Points (PP):** Equal to POW; used for Knacks.

## Choose Skills

Skills are things a character specializes in doing.
At the beginning of play, a character has 250 percentage points to spend on skills.
In Hundred Hack there is no defined skill list, but here are a few examples for inspiration:

> Dodge, Archery, Fencing, Culture (English), Lore (Alchemy), Athletics, Sculpting, Driving, Influence, Mechanisms, Streetwise, Trade.

The highest relevant skill bonus is always added to tests.

## Determine Wealth and Buy Equipment

Roll 3D6 to determine your starting wealth:

|  3D6  | IP | Description | Social Class               | Usage Die |
|:-----:|:--:|:-----------:|:--------------------------:|:---------:|
|   3   | 1  | Destitute   | Beggars and serfs          | D4        |
|  4-6  | 4  | Poor        | Freemen laborers           | D6        |
|  7-10 | 7  | Average     | Freemen with a trade       | D8        |
| 11-14 | 11 | Well-off    | Minor merchants, officials | D10       |
| 15-17 | 15 | Wealthy     | Merchants, minor nobility  | D12       |
| 18    | 18 | Rich        | Kings, powerful nobles     | D20       |

Whenever you try to buy something outside your normal means, roll your usage die.
As your Usage die shrinks, so does your Wealth.

## Determine Relationships
When you begin play, choose an ally, a dependent, and either an enemy or an organization.
You have a relationship bonus with each of these equal to 25%.

+ **Ally**. This is a group or person a player character can ask for aid, can call in favors from, or with whom they otherwise have a positive relationship.
+ **Dependent**. This is usually the character's family or a group that relies upon them.
+ **Enemy**. This is a sworn enemy. Interactions with this person or group are usually negative. They may be a recurring foe or ethnic enemy.
+ **Organization**.This could be a Wizard's Fraternity, a Former Regiment or even a Cult. The character has an association with this organization; they typically deal with this group on a favorable basis. GMs should determine the maximum scale of an organization, which could be as great as a whole nation, or just a small wandering band of minstrels.

On a Critical success for a relationship test, the character gains a very positive response, and an increase of 1D6% to their relationship.
On a fumble the character damages their relationship with the party and loses 1D4% from their skill.
Further, after a fumble, the player cannot call on a relationship for rest of the adventure.
In the case of enemies this means that they have accidentally aided them in some way.

| Relationship | IP Cost | Advantages (must make a successful relationship roll) | Disadvantages |
|:------------:|:-------:|:------------------------------------------------------|:--------------|
| Ally         | 2       | Loan an appropriate item or finances; assist on social test; provide info and/or employment | Allies demand favors of the character, in the form of money, errands etc for each favor given; enemies of your ally become your enemies.
| Dependent    | 1       | Characters gain 1-3 additional Improvement Points where they support, aid or defend Dependents. | Dependents regularly get themselves and the character into trouble; Enemies will target dependents to harm the character
| Enemy        | 2       | Gains +25% to tests in the attempt to thwart their enemy;  gain 1-3 additional IP for thwarting an enemy's plans; gain employment by enemy's opponents | Enemies will try to harm characters as often as possible; All interactions (parleys, influence, negotiation) with an enemy will be at one difficulty rating harder than normal.
| Organization | 2       | Can access the facilities offered by the organization; can gain information and possible employment | Organizations typically demand a charge for their services, either in cash or a favor; Enemies of an organization may harry and harm the character for their relationship to the organization. 

You can purchase new relationships later with Improvement Points for the cost listed; new relationships start at a bonus of 10%.

## Set Goals
At the start of play, give your character two long term goals and one short term goal.

Long term goals are things that are life goals, only achievable over the period of a linked set of adventures, commonly known as a ‘campaign'.
Short term goals are usually relevant to the adventure currently being played, and are determined near the beginning of the session by the players.

Goals help you earn improvement points:

+ Every time a goal is brought into play in a concrete way, the character earns one improvement per session.
+ Short term goals are removed at the end of the session, and if completed earn an additional two improvement points.
+ When a long term goal is finally achieved it is removed from the player's character sheet and the character earns five improvement points.

## Armor Dice

Armor provides protection by reducing all incoming damage.
When successfully attacked, roll the appropriate die and reduce the incoming damage by the result.

|     Type     | Points | 
|:------------:|:------:|
|   Gambeson   |   D4   |
|   Lamellar   |   D6   |
|    Plate     |   2D4  |

## Weapons and Shields
Weapons and shields have sizes - when a character uses their reaction to parry an attack, they can negate all of the damage on a successful parry if their weapon or shield is the same size; if the parrying weapon/shield is one size smaller it blocks half the damage; if the parrying weapon/shield is more than one size smaller it blocks none of the damage.


| ITEM                     | USAGE DIE |           NOTES            |
|:-------------------------|:---------:|:--------------------------:|
| Gambeson                 |   d10     |         D4 Armor           |
| Lamellar                 |   d12     |         D6 Armor           |
| Plate                    |   d20     |         2D4 Armor          |
| Small 1H Weapon          |    d8     |        1D6 Damage          |
| Medium 1H Weapon         |    d8     |        1D8 Damage          |
| Large 2H Weapon          |    d8     |        2D6 Damage          |
| Small Shield             |    d6     |        D4 Damage           |
| Medium Shield            |    d8     |        D6  Damage          |
| Large Shield             |   d10     |        D8  Damage          |
| Huge Shield              |   d12     |        D10 Damage          |
| Backpack                 |     -     |       Carry +2 extra       |
| Flask of oil             |    d6     |             -              |
| Tools                    |     -     |   Related tests are easy   |
| Lantern                  |     -     |             -              |
| Handheld mirror          |     -     |             -              |
| Preserved Rations        |    d8     |             -              |
| Fresh Rations            |    d4     |             -              |
| 50' Rope                 |     -     |             -              |
| Small Sack               |     -     |             -              |
| Large Sack               |     -     |             -              |
| Flint & Steel            |     -     |             -              |
| Torches (6)              |    d6     | Each Torch has a Usage Die |
| Wineskin                 |    d6     |             -              |
| Assorted Common Herbs    |    d8     |             -              |
| 10' Pole                 |     -     |             -              |
| Quiver of Arrows / Bolts |    d10    |             -              |


## Players turn

During a player’s turn a character may move, perform an action, and (if appropriate) perform a reaction.
They could attack, look for a clue, talk with an NPC, cast a spell - interacting with the world is an action.
Often they will test their attributes to determine the outcome.

## Time & turns

There are 2 important types of tracked time - **Moments** (*rounds*) and **Minutes** (*turns*).
**Moments** are used during combat and fast paced scenes of danger and **Minutes** are used when exploring and adventuring.
A GM may advance the clock as they need substituting **Minutes** for **Hours**, **Days** or even **Months** should the adventure require it.

## Movement & distance

Rather than track precise numbers, 100H uses 4 abstract ranges for measuring distances. **Close**, **Nearby**, **Far-Away** and **Distant**.
On their turn every character can move somewhere **Nearby** as part of an action, performing that action at any stage of the move.
They can forgo their action and move somewhere **Far-Away** instead.
Anything beyond **Far-Away** can be classified as **Distant** and would take 3 moves to get to.

This system is designed to support the narrative ‘theatre of the mind’ style of play, and is less concerned about tracking squares and fiddly distances.
For converting existing movement rates or measures (for spells or areas of effect) use the following as a guide :

|  CLOSE  |  NEARBY  |  FAR AWAY  |
|:-------:|:--------:|:----------:|
| 0 - 5ft | 5 - 60ft | 60 - 120ft |

## Initiative

When combat breaks out, everyone must be sorted into an order so they each get to act and react in turn.
Every character tests their **DEX** or **INS**.

The characters that fail declare their actions first, from lowest to highest roll.
Then characters that succeed declare their actions, from lowest to highest roll.
Finally, characters that critically succeed, from lowest to highest.

Once all actions are declared, they are resolved in reverse.

## Attacking, defending & damage

When a character attacks a creature they must roll below their active **BOD** score for a **Melee Attack** or **DEX** for a **Ranged Attack**. 
Likewise, when a creature attacks, the character must roll below its reactive **BOD** score against a Melee Attack and **DEX** against a Ranged Attack to avoid taking damage.

## Major Wounds

| Result on 1D10 | Major Wound |
|:--------------:|:-----------:|
|        1       | _Lose an eye_. All visual Perception tests are now hard, lose 4 points of DEX, 1 point of POW permanently.
|        2       | _Cracked skull_. Lose 4 points of INS. All skills involving mental processes become -25 permanently.
|        3       | _Left Leg crippled_. Fall prone, move half speed. Lose 2 points of DEX and 2 points of BOD permanently.
|        4       | _Right Leg crippled_. Fall prone, move half speed. Lose 2 points of DEX and 2 points of BOD permanently.
|        5       | _Broken ribs_. All skills are hard, due to severe pain.
|        6       | _Slashed stomach_. Lose one extra hit point per round from blood loss. Lose 3 points of BOD permanently.
|        7       | _Heart stops in shock!_ Unconscious for D10 moments, fall prone and can not move. Lose 2 points of BOD permanently.
|        8       | _Spine broken_. Permanently paralyzed below the torso. Half BOD permanently.
|        9       | _Left Arm crippled_. Automatically drop any held items.
|        10      | _Right Arm crippled_. Automatically drop any held items.

## Dying

When a character is reduced to zero **Hit Points (HP)** they are dead.

## Healing

Characters can gain Hit Points from Spells, Potions, and Knacks.
They can never gain more than their maximum - and can never go below zero either (because they're dead).

## Resting

Once per day, after resting, characters regain all expended **PP**.
Additionally, characters can make a BOD test to regain **HP** - if successful, they regain 1D4 + 1 **HP**.
If unsuccessful, they regain only 1**HP**.

## Improving Characters
Characters in HundredHack advance in one of two ways:

1. By using their skills, characters have a chance to improve those skills.
2. By spending improvement points gained by accomplishing goals, characters can improve themselves.

### Improving Skills Through Use
The first time a character succeeds on a normal or hard test under duress which uses a skill, place a mark beside the skill.
At the completion of a quest or when beginning an interlude - whenever there's a sensible break in the quest to reflect and take time to recover - players should roll 1d100 for each skill they marked during play.
If the result of the roll is _greater than_ the bonus of the skill they're rolling for, they have improved it.
In this case, roll 1D4 and add it to the skill bonus.
This is the character's new skill bonus.

_Every time_ you fumble a test using a skill, add 1D4 to the skill.
This represents a character gleaning new understanding from failure. 

There is no limit to the bonus a skill can reach.

### Spending Improvement Points
Characters can spend improvement points to improve skills, characteristics, or purchase new knacks.

A player can choose to spend one improvement point to increase on skill bonus by 5 points.

A player can choose to spend three improvement points to increase one Characteristic by one point.
The maximum a human character can increase a Characteristic to is 21.
For non-humans, the maximum for a Characteristic is equal to the maximum possible starting score for the Characteristic plus three.

A player can spend one improvement point per magnitude to acquire or improve an knack.

### Improving via Practice or Research

During downtime the characters may improve their characters.
For each three month period of practice or research a character may gain 1 Improvement Point.

## Encumbrance

A character can carry a number of items equal to their **BOD** with no issues.
Carrying over this amount means they are encumbered and all tests are hard - they can also only ever move to somewhere **Nearby**.
They simply cannot carry more than double their **BOD**.

## Usage die

Any item listed in the equipment section that has a **Usage die** is considered a consumable, limited item.
When that item is used the next **Minute** (turn) its **Usage die** is rolled.
If the roll is 1 then the usage die is downgraded to the next lower die in the following chain:

**d20 > d12 > d10 > d8 > d6 > d4**

When you roll a 1 on a d4 the item is expended and the character has no more of it left.

----

## Knacks
Knacks are special abilities characters can possess.
The knacks are purposefully written to be generic, allowing them to be flavored however makes sense for your character.

Unless otherwise stated all Knacks have the following traits.
+ They have Variable Magnitude.
  This means that the Magnitude of the knack starts from the stated Magnitude and then can be used at a higher Magnitude, if the character knows it, giving an increase in the effect of the knack.
  The maximum Magnitude that a character can learn is equal to their Power score.
+ Base Magnitude is one.
+ Range is Nearby.
+ All knacks, unless noted, have a Duration of ten **minutes**.
+ Other traits used by knacks are detailed below.
  + Area (X): The knack affects everything within the specified distance, centered on a target point.
  + Concentration: The knack's effects will remain in place so long as the character continues to concentrate on it.
  + Instant: The knack's effects take place instantly.
  + Magnitude (X): The strength and power of the knack and the minimum number of Power Points required to use it.
  + Non-Variable: The knack may only be used at the stated Magnitude.
  + Permanent: The knack's effects remain in place until they are dispelled or dismissed.
  + Passive: The knack is always in effect and cannot be dispelled or dismissed.
  + Prerequisites: This knack has one or more prerequisite conditions which must be met before the knack can be acquired.
  + Resist (Dodge/Persistence/Resilience): The knack's intended effects do not succeed automatically and the target may make an opposed test to avoid them entirely.
  + Touch: Touch knacks require the character to actually touch their target for the knack to take effect, using a close combat skill test to make contact.

| Knack                                                 | Traits                                                         | Effect |
|:-----------------------------------------------------:|:--------------------------------------------------------------:|:-------|
| [Animal Whisperer](#animal-whisperer)                 | Magnitude 2, Non-Variable, Touch                               | Character whispers into the ear of a distressed animal, calming it.
| [Avoidance](#avoidance)                               | Instant                                                        | Allows character to Dodge as many times as the knack's Magnitude.
| [Babel](#babel)                                       | Magnitude 2, Non-Variable, Resist (Persistence)                | Garbles the language of the affected creature.
| [Bearing Witness](#bearing-witness)                   | Instant                                                        | $$10\%$$ per Magnitude bonus to next Skill Test.
| [Beast Call](#beast-call)                             | Magnitude 2, Non-Variable, Instant, Resist (Resilience)        | Attract an animal within range.
| [Befuddle](#befuddle)                                 | Magnitude 2, Non-Variable, Resist (Persistence)                | Confuses the target so they cannot attack.
| [Block Sense (Sense)](#block-sense-sense)             | Magnitude 3, Non-Variable, Resist (Persistence)                | Blind/Deafen/Bland taste/Numb touch.
| [Care](#care)                                         | Magnitude 2, Non-Variable, Touch                               | Recipient gets benefit of character's Protection or Countermagic knacks.
| [Clear Path](#clear-path)                             | Touch                                                          | Clears teh way through dense undergrowth.
| [Coordination](#coordination)                         | Touch                                                          | $$+10\%$$ per Magnitude to Initiative, Dodge and Athletics
| [Counter-Attack](#counter-attack)                     | Magnitude 2, Non-Variable                                      | Allows a counter-attack in addition to any other Reactions.
| [Counter-Defence](#counter-defense)                   | Magnitude 2, Non-Variable                                      | Allows an extra reaction in addition to any other reactions.
| [Counterknack](#counterknack)                         | Instant                                                        | Used as a combat reaction, automatically dispels any knack used within range whose Magnitude is lower or equal to its own.
| [Cover Blind Side](#cover-blind-side)                 | Magnitude 1, Non-Variable                                      | Recipient can react to attacks from behind as if it was a normal attack from front.
| [Create Charms](#create-charms)                       | Permanent                                                      |  Create magic items imbued with some measure of a knack.
| [Create Power Point Store](#create-power-point-store) | Permanent                                                      | Create a Power Point Store which can later be used instead of the character's own Power Points. One point per Magnitude.
| [Create Potion](#create-potion)                       | Permanent                                                      |  Creates potions imbued with some measure of a knack.
| [Cushion Fall](#cushion-fall)                         |                                                                | Each point of Magnitude elimintates one dice of falling damage for the recipient.
| [Darkwall](#darkwall)                                 | Area (Close), Magnitude 2, Non-Variable                        | Create a moveable solid wall of darkness.
| [Demoralize](#demoralize)                             | Magnitude 2, Non-Variable, Resist (Persistence)                | If used before combat begins, target loses will to fight. If used during combat, all tests for target are Hard and they may not utilize offensive knacks.
| [Detect X](#detect-x)                                 | Concentration, Non-Variable                                    | Where X is a substance or a group of living beings.
| [Dispel Magic](#dispel-magic)                         | Instant                                                        | Dispels spells of a Magnitude equal or lower to it.
| [Disruption](#disruption)                             | Instant, Resist (Resilience)                                   | Causes $$1\text{D}4$$ damage per Magnitude
| [Dragon's Breath X](#dragons-breath)                    | Magnitude 2, Non-Variable, Instant, Resist (Dodge)             | Character breaths a stream of fire for $$1\text{D}10$$ damage.
| [Dull Weapon](#dull-weapon)                           |                                                                | Reduce a weapon's damage by one point per Magnitude.
| [Enhance Skill X](#enhance-skill-x)                   |                                                                |  Adds $$+10\%$$ to a particular skill.
| [Extinguish](#extinguish)                             | Instant                                                        | Puts out fires.
| [Extra Defence](#extra-defense)                       | Ranged                                                         | Each point gives an extra defensive reaction.
| [Fanaticism](#fanaticism)                             | Magnitude 2, Non-Variable                                      | $$+25\%$$ to close combat skill but may not parry or cast spells.
| [Elemental Arrow](#elemental-arrow)                   | Magnitude 2, Non-Variable                                      | Affected arrow does $$1\text{D}10$$ magical flame damage.
| [Elemental Blade](#elemental-blade)                   | Magnitude 4, Touch, Non-Variable                               | Affected weapon does $$1\text{D}10$$ magical flame damage.
| [Fist of the Wind](#fist-of-the-wind)                 | Instant                                                        | One extra unarmed / grapple attack per Magnitude.
| [Flying Kick](#flying-kick)                           | Magnitude 2, Non-Variable                                      | Normal move through air, followed by an attack.
| [Frostbite](#frostbite)                               | Magnitude 2, Non-Variable, Instant, Resist (Resilience)        | $$1\text{D}8$$ cold damage to target.
| [Glue](#glue)                                         | Touch, Area (Special)                                          | Covers an area with extremely sticky glue.
| [Hand of Death](#hand-of-death)                       | Instant, Magnitude 4, Non-Variable, Resist (Resilience), Touch | Reduced Hit Points by half and inflicts a Major Wound.
| [Harden](#harden)                                     | Magnitude 1, Non-Variable, Instant                             | Makes the target item unbreakable.
| [Heal](#heal)                                         | Instant, Touch                                                 | +1 Hit Point per point of magnitude Magnitude, but must understand what they're healing.
| [Hinder Skill X](#hinder-skill-x)                     | Ranged, Resist (Persistence)                                   | $$-10\%$$ to target's given skill per point of Magnitude.
| [Ignite](#ignite)                                     | Magnitude 1, Non-Variable, Instant                             | Starts fires.
| [Invisibility](#invisibility)                         | Magnitude 5, Non-Variable, Concentration, Touch, Personal      | Make the recipient invisible.
| [Ironmind](#ironmind)                                 | Touch                                                          | $$+10\%$$ per Magnitude vs Spells and Knacks.
| [Knockback](#knockback)                               | Instant, Resist (Resilience)                                   | Knocks victim back Magnitude in yards.
| [Knockdown](#knockdown)                               | Instant, Magnitude 2, Non-Variable, Resist (Resilience)        | Knocks target prone.
| [Leap](#leap)                                         | Touch, Resist (Dodge)                                          | Jump 2 yards up in air per point of Magnitude.
| [Light](#light)                                       | Magnitude 1, Non-Variable, Area (Nearby)                       | A magical light that illuminates its area.
| [Mindspeech](#mindspeech)                             |                                                                | Allows mental communication with one target per point of Magnitude.
| [Mobility](#mobility)                                 |                                                                | +2 yards to Movement Rate per Magnitude
| [Multi-Attack](#multi-attack)                         | Instant                                                        | One extra attack per magnitude
| [Personal Insight](#personal-insight)                 | Magnitude 2, Non-Variable                                      | Answers one question relevant to the character.
| [Pierce](#pierce)                                     | Touch                                                          | -1 AP per Magnitude
| [Protection](#protection)                             |                                                                | +1 AP per Magnitude
| [Read Emotion](#read-emotion)                         | Instant, Magnitude 1, Non-Variable, Resist (Persistence)       | Allow the character to know the true emotional state of the Target
| [Resist (Element)](#resist-element)                   |                                                                | $$+10\%$$ per Magnitude to resist a given element.
| [Restore Energy](#restore-energy)                     | Touch, Instant                                                 | Each Magnitude of this knack restores one Fatigue level.
| [Sap Energy](#sap-energy)                             | Touch, Instant, Resist (Resilience)                            | Inflict one Fatigue level per Magnitude on the target.
| [Scare](#scare)                                       | Magnitude 2, Non-Variable, Resist (Persistence)                | Target must move away from the character
| [Second Sight](#second-sight)                         | Magnitude 3, Non-Variable                                      | Allows the character to judge how many Power POints a target has.
| [Skybolt](#skybolt)                                   | Instant, Magnitude 3, Resist (Dodge)                           | A bolt of electrical energy that does $$2\text{D}6$$ damage and ignores non-magical Armor.
| [Slow](#slow)                                         | Resist (Resilience)                                            | -2 yard to Movement Rate per Magnitude.
| [Speedart](#speedart)                                 | Touch, Magnitude 2, Non-Variable, Trigger                      | +3 damage, $$+25\%$$ to Ranged Combat skill
| [Strength](#strength)                                 | Touch                                                          | $$+10\%$$ to any strength based Athletics test and Damage per point of Magnitude
| [Tallk to Animal](#talk-to-animal)                    | Magnitude 3, Non-Variable                                      | Character can talk to any animal within ten yards
| [Thunder's Voice](#thunders-voice)                    |                                                                | $$+10\%$$ to Influence per Magnitude
| [Tongues](#tongues)                                   | Magnitude 2, Non-Variable                                      | Allows the recipient to talk a given language/
| [Unlock](#unlock)                                     | Touch, Instant                                                 | Unlocks locked items. Base chance = $$\text{Magnitude} * 20\%$$
| [Vigor](#vigor)                                       | Touch                                                          | +2 Hit POint per point of Magnitude for the duration of the spell.
| [Vomit](#vomit)                                       | Ranged, Resist (Resilience)                                    | Target vomits for Magnitude rounds, takes $$1\text{D}6$$ damage if resist roll fumbled.
| [Walk on (Element)](#walk-on-element)                 | Magnitude 3                                                    | Allows character to walk on given element without harm
| [Water Breath](#water-breath)                         | Touch                                                          | Allows character to breathe underwater.
| [Weapon Enhance](#weapon-enhance)                     | Touch                                                          | $$+10\%$$ to hit, +1 Damage with weapon cast on. Extra damage is magical.

### Animal Whisperer
+ **Traits:** Touch, Magnitude 2, Non-Variable
+ **Description:** The character whispers into the ear of a distressed animal, calming it.
  If the distressed animal is under the influence of a knack such as Fear or Scare, then its gets another Persistence test to shake off the effect of the knack.

### Avoidance
+ **Traits:** Instant
+ **Description:** This knack lies dormant until the recipient is attacked.
  Then, after the normal reaction of the recipient, it fires off allowing the recipient to Dodge a number of times equal to the knack's Magnitude.
  Once triggered, all the points of the spell are fired off at once.

### Babel
+ **Traits:** Magnitude 2, Non-Variable, Resist (Persistence)
+ **Description:** This knack garbles the language of the affected creature.
  The target can still think and, for the most part, act normally, but anything it says comes out as gibberish.
  Thus, a commanding officer would be unable to give orders to his men and a spellcaster would be unable to cast spells.

### Bearing Witness
+ **Traits:** Instant
+ **Description:**This knack grants the character a +10% bonus per point of Magnitude to their next Skill Test they make to discover lies, secrets, or hidden objects.
It does not stack with any other knack bonuses.

### Beast Call
+ **Traits:** Instant, Magnitude 2, Non-Variable, Resist (Resilience)
+ **Description:** This knack serves to attract an animal which is far away or closer.
  When the knack is used it affects a targeted creature with a fixed INS of 7 or less.
  If it fails to resist, the creature will be naturally drawn to the place where the knack is used, whereupon the effect terminates.
  Any barrier, immediate threat, or counter control, also ends the effects of the knack, leaving the creature to react naturally.

### Befuddle
+ **Traits:** Magnitude 2, Non-Variable, Resist (Persistence)
+ **Description:** This knack prevents a target from using knacks and taking offensive actions.
  The target may run if it so chooses and may dodge and parry normally in combat, though it may not make any attacks unless it is attacked first.

### Block Sense (Sense)
+ **Traits:** Magnitude 3, Non-Variable, Resist (Persistence)
+ **Description:** This knack will Blind/Deafen/Bland taste/Numb touch on a failed resistance roll for the duration of the knack.

### Care
+ **Traits:** Touch, Magnitude 2, Non-Variable
+ **Description:** If the character has any active Protection or Countermagic knacks, the Cared for character _also_ benefits fromt he effects of these knacks.

### Clear Path
+ **Traits:** Touch
+ **Description:** This knack allows the character to move through even the most tangled, thorny brush as if they were on an open road.
  For each additional point of Magnitude, they may bring one person with them.

### Coordination
+ **Traits:** Touch
+ **Description:** For every point of Magnitude the target gains a +10% bonus to initiative, dodge, and DEX-based Athletics tests.

### Counter-Attack
+ **Traits:** Magnitude 2, Non-Variable
+ **Description:** This knack lies dormant until the recipient is attacked.
  Then, after the normal defensive reaction of the recipient, it fires off, allowing the recipient to follow up with a counter attack.
  The counter attack is an additional action, on top of the recipients normal attacking action.

### Counter-Defense
+ **Traits:** Magnitude 2, Non-Variable
+ **Description:** This knack lies dormant until the recipient is attacked.
  Then, after the normal defensive reaction of the recipient, it fires off, allowing the recipient an extra defensive reaction, such as a dodge or parry.

### Counterknack
+ **Traits:** Instant
+ **Description:** This knack is only used as a Reaction, and only when a knack is used within Counterknack's Range that the character wishes to counter.
  It must be declared during the initiative phase as normal.
  Counterknack disrupts the other knack and nullifies it as long as Counterknack's Magnitude equals or exceeds the target knack's Magnitude.

### Cover Blind Side
+ **Traits:** Magnitude 1, Non-Variable
+ **Description:** For the duration of the knack the target can react to attacks from behind or flank attacks as if they were a normal attack from the front.
  It does not confer any additional reactions.

### Create Charms
+ **Traits:** Permanent
+ **Description:** A charm is a physical item that stores one or more knacks that the character possesses.
  A charm is a representation of the character's incredible skill and abilities when applied to artisanry.
  A charm could be a necklace that holds a Befuddle 4 knack, a sword invested with the will of its smith and so holds a Weapon Enhance 2 knack, or even a sheet of paper with a poem writton it that, when held agains the skin, provides a Protection 1 knack.

  To create a charm a character must possess both the knack they wish to imbue and Create Charm at the same Magnitude.

  If the character spends one Improvement Point at the time of creation the knack within the Charm is reusable.
  Otherwise, once the knack is used, the Charm loses all potency.

  A knack stored in a Charm is used like any other Knack the possessor has acquired.
  It is powered by the wielder's Power Points.

  The time taken to create a single-use Charm is one hour per point of Magnitude of the knack being imbued.
  Reusable Charms take three hours per point of Magnitude to create.

  Charms are mundane items in their own right and if the item is broken the Charm is lost.

### Create Power Point Store
+ **Traits:** Permanent
+ **Description:** This knack allows the character to create an item which as Power Point storing capabilities.
  This allows the owner to have a pool of Power Points in addition to their own.
  This also applies to charms, such as a sword with Weapon Enhancement 2 stored in it, to provide a pool of power points to use the knack from.

  Power Point stores take one hour per power point stored in them to create.
  For each Magnitude, one power point can be stored.

  The character must fill the item with their own Power Points as part of the knack.
  The amount of Power Points put into the item at the time of casting becomes the maximum that can be put into the item.

  Once the Power Points are used the item loses its ability to store Power points.
  
  If the improvement point is spent at creation the item then becomes reusable.
  Once all the Power Points are used, the item can be refilled instantly from the user's own Power Points.

  If the item is destroyed the Power points are released harmlessly into the surrounding area.

### Create Potion
+ **Traits:** Permanent
+ **Description:** Potions are liquids that store one or more Knacks.
  The Magnitude of the Create Potion knack needs to equal or exceed the highest Magnitude of the knack being stored into the potion.

  All potions are one use.
  They must be drunk in one swift gulp to work.

  The potion automatically works and doesn't incur a cost in power points to the person who is drinking it.

  The potion costs the maker power points.
  They must have the knack at the Magnitude they are creating the potion at, with the power points of the knack being put into the potion.

  To make the potion, the character must roll successfully against Knackery for each knack being placed in the potion and against Lore (Potion Making).
  If they fail, the potion is ruined and they spend the power points they attempted to invest in the potion.

  Potions take one hour per point of Magnitude of knack(s) stored to create.

  A potion must be stored in an air tight container, or it evaporates, losing one point of Magnitude per week.

### Darkwall
+ **Traits:** Area (Close), Magnitude 2, Non-Variable
+ **Description:** Light sources within a Darkwall area shed no light nad normal sight ceases to function.
  Other senses such as a bat's sonar function normally.

  The character may move the Darkwall each **moment**.
  If this option is chosen, the knack gains the Concentration trait.

### Demoralize
+ **Traits:** Magnitude 2, Non-Variable, Resist (Persistence)
+ **Description:** This knack creates doubt and uncertainty into the very heart and soul of the target.
  All close combat and ranged combat tests for the target are Hard and the target may not cast offensive spells nor use offensive knacks.
  If this knack takes effect before combat begins the target will try to avoid fighting and will either run or surrender.
  The effects of this knack are automatically cancelled by the Fanaticism knack and vice versa.

### Detect X
+ **Traits:** Magnitude 1, Concentration, Non-Variable
+ **Description:** This covers a family of knacks that all operate in a similar fashion, allowing the character to locate the closest target of the knack within its range.
  This effect is stopped by a substances over a yard thick, such as castle walls or the ground between caves.
  It is also blocked by Counterknack, though the character will know the target is somewhere within range (though not its precise location) and that it is being protected by Counterknack.
  The separate Detect knacks are listed below nad each must be learned separately.
  + **Detect Enemy:** Gives the location of the nearest creatures that intend to harm the character.
  + **Detect Magic:** Gives the location of the nearest magic item, magical creature, or active knack.
  + **Detect Species"** Each Detect Species knack will give the location of the nearest creature of the specified species.
  + **Detect Substance:** Each Detect Substance knack will give the location of the nearest substance of the specified type.

### Dispel Knack
+ **Traits:** Instant
+ **Description:** This knack will attack and eliminate active knacks.
  Dispel Knack will eliminate a combined Magnitude of knacks equal to its own Magnitude, starting with the most powerful affecting the target.
  If it fails to eliminate any knack because the knack's Magnitude is too high, then its effects immediately end and no more knacks will be eliminated.
  Knacks cannot be partially eliminated so a target under the effects of a knack whose Magnitude is higher than that of Dispel Knack will not have any knacks currently affecting it eliminated.

### Disruption
+ **Traits:** Instant, Resist (Resilience)
+ **Description:** Disruption literally pulls a target's body apart.
  The target will suffer 1D4 points of damage per point of Magnitude, ignoring armor.

### Dragon's Breath X
+ **Traits:** Instant, Magnitude 2, Non-Variable, Resist (Dodge)
+ **Description:** This includes a number of different knacks, each of which allows the character to spit a stream of a specific element at their target.
  If the breath attack is not dodged, it inflicts 1D10 points of damage.
  Armor is effective against this damage and it counts as both magical and the appropriate elemental damage.

### Dull Weapon
+ **Traits:** None
+ **Description:** This knack can be used on any weapon.
  For every point of Magnitude it reduces the damage dealt by the target weapon by one.

### Enhance Skill (X)
+ **Traits:** None
+ **Description:** This includes a number of different knacks, each of which affects a different non-combat skill.
  For each point of Magnitude, the recipient gains +10% to any skill test using the Enhanced skill.
  Alternatively, for each additional point of Magnitude of the knack, the character can effect one more target.
  The bonuses and targets can be split as necessary, providing each bonus in multiples of 10% and the total of bonuses equals the knack's Magnitude times 10%

### Extinguish
+ **Traits:** Instant
+ **Description:** This knack instantly puts out fires.
  At Magnitude 1 it can extinguish a flame, at Magnitude 2 a small fire, at Magnitude 3 a large fire, and at Magnitude 4 will put out an inferno.

### Extra Defense
+ **Traits:** Ranged
+ **Description:** Each point of Magnitude allows the target to make one extra close combat defensive reaction per combat round.

### Fanaticism
+ **Traits:** Magnitude 2, Non-Variable
+ **Description:** The target of this knack will have their close combat increased by 25%

### Farsight
+ **Traits:** Concentration
+ **Description:** Each Magnitude of this Knack extends the character's field of vision by ten yards as long as they maintain their concentration.
  Although they can see small details at a distance, this spell does not let the character see through walls or other obstructions.

### Elemental Arrow
+ **Traits:** Touch, Trigger, Magnitude 2,  Non-Variable
+ **Description:** Exercising this knack on a missile will cause it to burst into a given element (Air/Darkness/Earth/Fire/Water etc.) when it is fired and strikes a target.
  When it hits a target, the missile will deal 1D10 points of magical damage of the appropriate element instead of its normal damage.
  Since Elemental Arrow does magical damage, it affects creatures that are immune to normal damage.
  A missile under the effects of Elemental Arrow cannot benefit from Speedart.

### Elemental Blade
+ **Traits:** Touch, Magnitude 4, Non-Variable
+ **Description:** For the duration of the spell, the target weapon will deal 1D10 magical damage of the given element (Air/Darkness/Earth/Fire/Water etc.) instead of its normal damage.
  A weapon under the effects of Elemental Blade cannot benefit from Bladesharp.
  Since Elemental Blade does magical damage, it damages creatures immune to normal damage.

### Fist of the Wind
+ **Traits:** Instant
+ **Description:** Each point of Magnitude allows the character to make one extra unarmed close combat attack or grapple.

### Flying Kick
+ **Traits:** Magnitude 2, Non-Variable
+ **Description:** This knack allows the recipient to make a normal move as a flying leap through the air, then make a Kick attack at the end of the move.

### Frostbite
+ **Traits:** Instant, Magnitude 2, Non-Variable, Resist (Resilience)
+ **Description:** This attack knack allows the character to freeze his opponent, dealing $$1\text{D}8$$ points of damage, ignoring any AP.
  Magical damage that protect against cold damage can block this effect but mundane items (such as cold weather gear) are ineffective.

### Glue
+ **Traits:** Touch, Area (Special)
+ **Description:** This knack covers an area of one inch square for each Magnitude with extremely sticky glue.
  If a creature steps on the glue, it must make an Athletics roll vs the Magnitude times 10% to avoid being stuck for one round.
  On subsequent rounds it  must make the same roll to break free.
  This knack can also be used for more conventional repairs, a broken sword for example, with Magnitude times 10% being the chance that the item won't break again, if used in circumstances that would cause it to.

### Hand of Death
+ **Traits:** Touch, Instant, Magnitude 4, Non-Variable, Resist (Resilience)
+ **Description:** This fearsome knack allows the character to deal an awful wound with the merest touch.
  Using the _Hand of Death_ charges the character with the knack.
  Touching an unsuspecting target, or succeeding at an Unarmed attack against a wary target, releases the knack's effect.
  If the Resilience test to resist the effect is failed, the victim immediately loses half their maximum Hit Points, and suffers a a Major Wound.
  If the Resilience test is a success, the target only loses 1D4 Hit Points.
  Armour does not protect against this damage.

### Harden
+ **Traits:** Touch, Magnitude 1, Non-Variable
+ **Description:** This knack makes a target item unbreakable for the duration of the knack.
  Therefore weapons with this knack used on them will not break when a Fumble is rolled in combat, and it allows items that are normally too brittle to be wielded in combat to be used as improvised weapons.

### Heal
+ **Traits:** Touch, Instant, Magnitude 2
+ **Description:** For every two points of Magnitude, the character can repair one Hit Point to damage of either themself or another target, but only if the character has sufficient understanding of the damage and what's required to heal the injured character.
  A Magnitude 6 or higher Heal knack will also re-attach a severed limb if used within ten rounds of the loss.
  A Magnitude 5 or higher Heal knack will also cure any single poison or disease affecting the target, but this does not negate the requirement to understand the disease / poison.

  In effect, the limitation on this knack prevents a character from healing another creature of an injury or disease they do not understand.
  If a character succeeds on a Lore (Medicine) test, they understand the injury or disease well enough to heal it.

### Hinder Skill (X)
+ **Traits:** Ranged, Resist (Persistence)
+ **Description:** This is a number of different knacks, each of which affects a different skill.
  For each point of Magnitude of the knack, the target gains a -10% penalty to the next test using the affected skill.

  Alternatively, for each additional point of Magnitude of the knack, the character can affect one more target.
  The bonuses and targets can be split as necessary providing each penalty is in multiples of 10% and the total of bonuses equals the spells Magnitude times 10%.
  If used in this way, each target is affected separately; if one target succeeds on resisting the knack, other targets may fail and be affected.

### Ignite
+ **Traits:** Instant, Magnitude 1, Non-Variable
+ **Description:** This knack will set fire to anything flammable within range, creating a flame.
  Skin or flesh cannot be ignited and if the target is attached to a living being (such as hair, fur or clothes) then the spell gains the Resist (Resilience) trait.

### Invisibility
+ **Traits:** Personal, Touch, Concentration, Magnitude 4, Non-Variable
+ **Description:** For the duration of the knack the recipient is completely invisible to sight.
They can still be heard, felt or smelled, with a -25% to Perception tests.
Also, the knack is automatically dispelled if the character loses concentration, or the recipient uses a knack or makes an attack.
The recipient also becomes visible immediately after the knack ends, so even if the character immediately uses another Invisibility knack there will be a delay between uses where the recipient is visible.

### Ironmind
+ **Traits:** Touch
+ **Description:** This knack hardens the resolve of the character that it is used upon for its duration.
  Each point of Magnitude of the knack adds 10% to all Persistence tests against attacks to the mind (e.g. Fear, Befuddle etc.) or opposed tests vs Influence.

### Knockback
+ **Traits:** Instant, Magnitude 3, Non-Variable, Resist (Resilience), Range (Close)
+ **Description:** The target of this knack is knocked back from Close to Nearby.

### Knockdown
+ **Traits:** Instant, Magnitude 2, Non-Variable, Resist (Resilience)
+ **Description:** The target of this knock is knocked down prone.

### Light
+ **Traits:** Magnitude 1, Non-Variable, Area (Nearby)
+ **Description:** used on a physical object (including living material), this knack causes the object to shed light across the area of effect.
  Note that only the specified area is illuminated – everything outside the area of effect is not.
  This knack creates raw light, not a flame.

### Mindspeech
+ **Traits:** None
+ **Description:** This knack can affect one target for every point of Magnitude, allowing telepathy between the character and any target, though targets will not have telepathy with one another.
  The words transmitted by telepathy must be whispered and will be heard directly in the head of the recipient, in the same language in which it was spoken. 

### Mobility
+ **Traits:** Instant
+ **Description:** For every point of Magnitude of this knack the target can move one distance further with a single movement, up to Distant.

### Multi-attack
+ **Traits:** Instant
+ **Description:** Each point of Magnitude allows the character to make one extra close-combat attack.

### Personal Insight
+ **Traits:** Magnitude 2, Non-Variable
+ **Description:** This knack gives the character or recipient a very direct insight into a small question directly relevant to them, in the form of an internal intuition.
For example the question 'Why can I not harm the creature?' would get the answer 'Because your sword is not enchanted', while 'Why can we not harm the creature?' would not get an answer. 

### Pierce
+ **Traits:** Touch
+ **Description:** This knack can be used on any weapon with a blade or point.
  For every point of Magnitude, it ignores one armour point when it strikes armour.
  Pierce can bypass magical armour as easily as normal armour.

### Protection
+ **Traits:** None
+ **Description:** For every point of Magnitude of this knack one armour point is added to the armour of the target.
  This stacks with any existing armour and is treated in the same way.

### Read Emotion
+ **Traits:** Instant, Magnitude 1, Non-Variable, Resist (Persistence)
+ **Description:** When used, this knack tells you what the true emotional state of the target is if they fail a Persistence roll.

### Resist Element
+ **Traits:** None
+ **Description:** This knack increases Resistance bonus against hostile effects, magic or otherwise, from a given element (Air/Darkness/Earth/Fire/Water etc.) by 10% per Magnitude, and subtracts 1 point of damage from that element per Magnitude.

### Restore Energy
+ **Traits:** Touch, Instant
+ **Description:** Each point of this knack's Magnitude instantly restores one fatigue level to the recipient.

### Sap Energy
+ **Traits:** Touch, Instant, Resist (Resilience)
+ **Description:** Each point of this knack's Magnitude instantly inflicts one fatigue level on the recipient.

### Scare
+ **Traits:** Magnitude 2, Non-Variable, Resist (Persistence) 
+ **Description:** The target is scared for 1D6 rounds.
  Scared targets must withdraw from combat with the character for the duration of the knack, and move as quickly as they are able, directly away from the character.

### Second Sight
+ **Traits:** Magnitude 3, Non-Variable
+ **Description:** Second Sight allows the character to gauge the POW of every creature and magic item within range.
  The knack is blocked by anything that blocks normal vision.
  The character will know if each aura created by the illuminated POW is less than their own POW, within three points of their own POW or greater than their own POW.

  Additionally, Second Sight provides a +25% bonus on Perception tests to notice hidden magical items or hiding people or creatures.
  Second Sight will also reveal invisible entities; though only a hazy image will show (treat such targets as partially obscured).

### Skybolt
+ **Traits:** Instant, Magnitude 3, Non-Variable, Resist (Dodge)
+ **Description:** The character summons a lightning bolt from the heavens regardless of the weather.
  The target must be outdoors in plain view.
  Skybolt inflicts 2D6 points of damage to a single chosen target.
  Only magical Armour Points offer protection against this damage

### Slow
+ **Traits:** Resist (Resilience)
+ **Description:** For every point of Magnitude of this knack the target's Movement speed will be reduced by one step.
A target's Movement may not be reduced to below Close through use of this knack.

### Speedart
+ **Traits:** Touch, Trigger, Magnitude 2, Non-Variable
+ **Description:** Used on a missile, this knack is triggered when it is fired.
  It gives a +25% to Ranged Combat and +3 damage while using the missile.
  A missile under the effects of Speedart cannot benefit from Elemental Arrow.

### Strength
+ **Traits:** 
+ **Description:** For every point of Magnitude of this knack, the target's Damage increases by +1 and strength based athletics tests are +10% per Magnitude.
  Note the Damage increase is not treated as magical damage. 

### Talk to Animal
+ **Traits:** Magnitude 3, Non-Variable
+ **Description:** With this knack the recipient is able to talk to any Nearby beast.
  This communication is verbal, therefore the recipient must be able to speak and be heard by the target animal.  

### Thunder's Voice
+ **Traits:** None
+ **Description:** For every point of Magnitude of this knack, the character has +10% added to their Influence skill bonus and can also be heard at up to one distance further away per Magnitude.

### Tongues
+ **Traits:** Magnitude 2, Non-Variable
+ **Description:** This knack allows the recipient to speak another language perfectly for its duration.

### Unlock
+ **Traits:** Touch, Instant
+ **Description:** This knack has a chance of opening a lock equal to the 20% per Magnitude, minus any modifiers due to the intricacy of the lock.

### Vigor
+ **Traits:** Touch
+ **Description:** For every point of Magnitude of this knack, the target's Hit Points increases by +2.
  A target cannot have its Hit Points increased in this way to more than twice its original value.
  Damage is taken from the ‘magical' Hit Points first, so when the knack dissipates the damage that was inflicted on the magical Hit Points disappear too.
  Recalculate the Major Wound level for the character while the knack is in effect. 

### Vomit
+ **Traits:** Ranged, Resist (Resilience)
+ **Description:** This knack incapacitates its Victim for 1 round per point of Magnitude, due to uncontrollable vomiting.
  On a fumbled resilience roll the Victim takes 1D6 Hit Points damage which armor can't reduce. 

### Walk On (Element)
+ **Traits:** Magnitude 3
+ **Description:** This knack allows the recipient to walk on the specified element (Air/Darkness/Earth/Fire/Water) without sinking or taking any harm from what is being walked on for the knack's duration.
  With this knack for the appropriate element, the character can walk across lava, quicksand, water, or even through the air.
  Each additional point of Magnitude increases the duration of the knack by 1 minute. 

### Water Breath
+ **Traits:** Touch
+ **Description:** This knack allows the target to breathe water for the duration of the knack.
  For every point of Magnitude, one additional person can be included in the knack, or increase the duration by one minute.
  Water Breath has no effect on the target's ability to breathe air.

### Weapon Enhance
+ **Traits:** Touch
+ **Description:** This knack can be cast on any close combat weapon or any unarmed attack.
  For every point of Magnitude, it increases the chance to hit with the weapon by +10% and deals one point of extra damage.
  This extra damage is magical and will affect creatures that can only be hurt by magic.
  The weapon's base damage remains non-magical.
  A weapon under the effects of this knack cannot benefit from Elemental Blade.

<!--
----

## Example of play

**DM :** So you're in the sewer, knee deep in muck, it's dark and to the north there's a portcullis, what do you want to do?
**Thief :** Is the portcullis Nearby?
**DM :** Yeah.
**Thief :** I'll move to it and as my action I’ll check it for traps.
**Warrior :** Assuming it's safe, I want to bend the bars.
**Conjurer :** And I want to cast light on my staff.
**DM :** Ok, Thief, test your Wisdom by rolling a d20 under your WIS score - to check the portcullis for traps.
**Thief :** \*rolls\* Made it!
**DM :** You're confident it's free of anything designed to do you harm, Warrior, still want to bend the bars? If so test your Strength!
**Warrior :** \*rolls\* Piece of cake!
**DM :** Good stuff, now Conjurer, you cast Light on your staff. That's a level 1 spell right?
**Conjurer :** Yup.
**DM :** Ok, well test your Intelligence and add one to your d20 roll, if you fail you lose a level one spell slot for the rest of the day.
**Conjurer :** \*rolls\* I need to roll under, not on it, right?
**DM :** That's right.
**Conjurer :** Damn, I failed.
**DM :** Unlucky! Beyond the bent iron bars is a long dark sewer tunnel heading deep down. What do you want to do?
**Warrior :** Explore down the tunnel..?
**Thief :** Agreed! I'll sneak ahead.
**Conjurer :** And I’ll protect the rear!
**DM :** Ok Thief, you move down the sewer, still Nearby to your friends, please test your Dexterity to see how quiet you are - remember you get Advantage on tests when sneaking, roll two d20's and choose the result you like.
**Thief :** \*rolls\* despite rolling two dice I’ve got a 17 and 19. What are the odds?
**DM :** Ouch. You're making so much noise being sneaky, a Ghoul hiding in the darkness close to you leaps and attacks!
**Thief :** Bugger!
**DM :** Initiative time! Everyone test their Dexterity, passing means you act before the Ghoul, failing means you go after. Thief you test with Disadvantage.
**Warrior :** I go before.
**Conjurer :** I'm after.
**DM :** Thief?
**Thief :** How long was it to roll up a character again? I go after.
**Warrior :** I want to run down the sewer and smash the Ghoul with my Broadsword.
**DM :** Ok Warrior, you move Close to the Ghoul. Test your Strength to see if you hit it, you should add +1 to the roll, as the Ghoul's a powerful opponent.
**Warrior :** \*rolls\* Rolled a 7! \*rolls again\* So that's 8HP damage.
**DM :** Good hit! Now the Ghoul's turn. Thief test your Dexterity to try and dodge the Ghoul's paralysing claws and bite. Remember the +1.
**Thief :** \*rolls\* Ugh! 18.
**DM :** Oh dear. You feel a painful numbing sensation run through your body. Test your Constitution, if you fail the Ghoul paralyses you.
**Thief :** \*rolls\* Do I add the +1 to this too?
**DM :** Yes. Conjurer, you see the Thief fall rigid to the floor, what do you do?
**Conjurer :** I'll start backing away slowly.
**Thief :** I'll get you in the next life you git!
-->